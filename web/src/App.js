import React from 'react';

import Routes from './routes.js';
import './global.css';
import './App.css';



function App() {
  return (
    <div id="app">
      <Routes/>
    </div>
  );
}

export default App;
