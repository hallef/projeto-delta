import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from './pages/home';
import Cadastro from './pages/cadastro';
import Menu from './components/menuHeader';

const Routes = () => (
  <BrowserRouter>
    <Menu/>
    <Switch>
        <Route path="/" component={Home} exact />
        <Route path="/cadastro" component={Cadastro} exact />
    </Switch>
  </BrowserRouter>
);

export default Routes;