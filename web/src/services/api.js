import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export const api = axios.create({
  baseURL: 'http://localhost:8080/',
});

function configureToast() {
  toast.configure({
    autoClose: 5000,
    draggable: false,
  });
}

export async function getAlunos() {

  configureToast();

  const result = await api({
    url: 'aluno/getAlunos',
    method: 'GET',
  }).catch((e) => {
    toast.error(e.message);
  });

  return result.data;

}

export async function getAluno(id) {

  configureToast();

  const result = await api({
    url: `aluno/${id}`,
    method: 'GET',
  }).catch((e) => {
    toast.error(e.message);
  });

  return result.data;

}

export async function getALunoNome(nome) {

  configureToast();

  const result = await api({
    url: `aluno/findAluno`,
    method: 'POST',
    data: nome
  }).catch((e) => {
    toast.error(e.message);
  });
  return result.data;

}

export function uploadFoto(foto) {

  return api({
    url: '/documento/upload',
    data: foto,
    method: 'POST'
  })

};

export async function deleteAluno(id) {

  configureToast();

  await api({
    url: `aluno/deletar/${id}`,
    method: 'DELETE',
  }).then(() => {
    toast.success('Aluno excluido com sucesso');
  }).catch((e) => {
    toast.error(e.message);
  });
}

export async function saveAluno(aluno) {

  configureToast();
  const result = await api({
    url: `aluno/created`,
    method: 'POST',
    data: aluno,
  });

  if (!result) {
    toast.error('Erro ao criar aluno');
  }

  toast.success('Aluno salvo com sucesso');
  return result.data;

}


export async function updateAluno(aluno) {

  configureToast();

  const result = await api({
    url: `aluno/update`,
    method: 'PUT',
    data: aluno,
  })

  if (!result) {
    toast.error('Erro ao atualizar aluno');
  }

  toast.success('Aluno atualizado com sucesso');

  return result.data;

}
