import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBrain, faAtom, faBalanceScale } from '@fortawesome/free-solid-svg-icons'

import './styles.css';

const cursos = [
  {
    id: 0,
    nome: 'SISTEMAS DE INFORMAÇÃO',
    tipo: 'BACHARELADO',
    icon: faBrain
  },
  {
    id: 1,
    nome: 'DIREITO',
    tipo: 'BACHARELADO',
    icon: faBalanceScale
  },
  {
    id: 2,
    nome: 'FÍSICA',
    tipo: 'BACHARELADO',
    icon: faAtom
  }
]

function home() {
  return (
    <div>
      <h1 className="titulo">Cursos</h1>
      <div className="container">
        {cursos.map( c => {
          return (
            <div className="container-curso">

              <FontAwesomeIcon className="img-curso white" icon={c.icon} />
              <h2 className="sub-titulo white">{c.nome}</h2>
              <p className="tipo white">{c.tipo}</p>

            </div>
          )
        })}
      </div>
    </div>
  );
}

export default home;