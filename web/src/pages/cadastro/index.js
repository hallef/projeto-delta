import React,  { useState, useEffect } from 'react';
import {deleteAluno, getAlunos, saveAluno, updateAluno, getAluno, getALunoNome } from '../../services/api';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

import FormularioAluno from '../../components/formularioAluno';
import Aluno from '../../components/aluno';

import './Sidebar.css';
import './Main.css';

const initialSchemaAluno = {
  usuario: '',
  nome: '',
  foto: 'https://i.stack.imgur.com/Hq8za.jpg',
  endereco: {
    cep: '',
    localidade: '',
    numero: '',
    bairro: '',
    complemento: '',
    logradouro: '',
    uf: ''
  }
}


function Cadastro() {

  const [aluno, setAluno] = useState(null);
  const [alunos, setAlunos] = useState([]);
  const [isEditar, setIsEditar] = useState(false);
  const [filtro, setFiltro] = useState('');

  useEffect(() => {
    async function loadAlunos() {
      const buscarAluno = await getAlunos();
      setAlunos(buscarAluno);
    }

    loadAlunos();
  }, []);

  async function filtrarAluno(nomeAluno) {
    setFiltro(nomeAluno);

    if (filtro) {
      const alunoBuscado = await getALunoNome({nome: nomeAluno});
      setAlunos(alunoBuscado);
    }
  }

  function handleFormReset (e)  {
    setIsEditar(false);
    e.preventDefault();
    setAluno(initialSchemaAluno);
  }

  async function handleAddAluno(data) {

    data.aluno = {
      ...data.aluno,
      endereco: data.endereco
    }
    if (isEditar) {
      setIsEditar(false);
      await updateAluno(data.aluno);
      const novosAlunos = await getAlunos();
      setAlunos(novosAlunos);
      setAluno();

    } else {

      const alunoSalvo = await saveAluno(data.aluno);
      if (alunoSalvo) {
        setAlunos([...alunos, alunoSalvo]);
      }

    }

  }

  async function editarAluno (idAluno) {

    setIsEditar(true);
    const aludoparaEditar = await getAluno(idAluno);
    setAluno(aludoparaEditar);

  }

  async function deletarAluno (idAluno) {
    confirmAlert({
      title: 'Excluir Aluno',
      message: 'Tem certeza que deseja excluir esse aluno?',
      buttons: [
        {
          label: 'Sim',
          onClick: async () => {
            if (aluno && aluno.id === idAluno) {
              setAluno(initialSchemaAluno);
            }
            setIsEditar(false);
            await deleteAluno(idAluno);
            const buscarAluno = await getAlunos();
            if (buscarAluno) {
              setAlunos(buscarAluno);
            }          
          }
        },
        {
          label: 'Não',
          onClick: () => {}
        }
      ]
    });
  };

  return(
    <div>

      <div className="div-filtro">
        <input 
          name="pesquisar" 
          id="pesquisar" 
          required
          value={filtro}
          placeholder="Filtrar por nome do aluno"
          onChange={e => filtrarAluno(e.target.value)}
          className="filtro"
        />
      </div>

      <div className="centralizar">
        <aside>
          <strong>Cadastrar aluno</strong>
          <FormularioAluno isEdit={isEditar} alunoEditar={aluno} handleFormReset={handleFormReset} onSubmit={handleAddAluno}  initialSchemaAluno={initialSchemaAluno} />
        </aside>

        {alunos.length > 0 ? (
          <main>
            <ul>
              {alunos.map(a => (
                <Aluno key={a.id} aluno={a} atualizar={editarAluno} deletar={deletarAluno}/>
              ))}
            </ul>
          </main>
        ) : (
          <div className="div-not-aluno">
            <p>Nenhum aluno encontrado</p>
          </div>)
        }
      </div>
    </div>
  );
}

export default Cadastro;