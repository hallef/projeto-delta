import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserEdit, faTrash } from '@fortawesome/free-solid-svg-icons'

import './styles.css';
import './tooltip.css';

function Aluno({ aluno, deletar, atualizar}) {
  return (
    <li className="aluno-item">
      <header className="header-content">
        
        <div className="user-info">
          <img src={`http://localhost:8080/documento/download/${aluno.foto}`} alt={`foto de ${aluno.nome}`} />
          <strong >{aluno.nome}</strong>  
        </div>
        <div className="div-endereco">
          <p>Endereço:</p>
          <div className="endereco">
            <p className="endereco-sub">Logradouro: {aluno.endereco.logradouro}, n° {aluno.endereco.numero}</p>
            <p className="endereco-sub">Bairro: {aluno.endereco.bairro}</p>
            <p className="endereco-sub">Cidade: {aluno.endereco.localidade} - {aluno.endereco.uf}</p>
          </div>    
        </div>

        <div>
          <div  className="div-acoes">
            <div className="div-row tooltip">
              <FontAwesomeIcon className="icon" icon={faUserEdit}  onClick={ () => atualizar(aluno.id)}  />
              <span className="tooltiptext">Editar aluno</span>
            </div>
            <div className="div-row tooltip">
              <FontAwesomeIcon className="icon" icon={faTrash} onClick={ () => deletar(aluno.id)} />
              <span className="tooltiptext">Excluir aluno</span>
            </div>
          
          </div>    
        </div>
      </header>
    </li>
  );
}

export default Aluno;
