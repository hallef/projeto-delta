import React, { useState, useEffect } from 'react';
import ImageUploader from 'react-images-upload';
import { uploadFoto } from '../../services/api';
import axios from 'axios';
import './styles.css';



function FormularioAluno({isEdit, onSubmit, alunoEditar, initialSchemaAluno, handleFormReset }) {
  const [aluno, setAluno] = useState(initialSchemaAluno);
  const [fotoPerfil, setFotoPeril] = useState(initialSchemaAluno.foto);
  const [endereco, setEndereco] = useState(initialSchemaAluno.endereco);

  async function uploadFotoMethod (part) {

    const formData = new FormData();
    formData.append("file", part[0]);
    const response = await uploadFoto(formData);
    setAluno({...aluno, foto: response.data})
    setFotoPeril(`http://localhost:8080/documento/download/${response.data}`);

  };

  useEffect(() => {

    if (alunoEditar) {
      if (alunoEditar.foto !== 'https://i.stack.imgur.com/Hq8za.jpg') {
        setFotoPeril(`http://localhost:8080/documento/download/${alunoEditar.foto}`);
      } else {
        setFotoPeril(initialSchemaAluno.foto)
      }
      setAluno(alunoEditar)
      setEndereco(alunoEditar.endereco)
    }

  }, [alunoEditar, initialSchemaAluno.foto])


  async function handleSubmit(e) {
    e.preventDefault();
    await onSubmit({
      aluno,
      endereco
    });

    limparCodigo();
  
  }


  function limparCodigo () {
    setAluno(initialSchemaAluno);
    setEndereco(initialSchemaAluno.endereco);
    setFotoPeril(initialSchemaAluno.foto);
  }

  async function findCorreios(cep) {
    cep = cep.replace(/\D/g, '');
    if (cep !== '') {
      var validacep = /^[0-9]{8}$/;
      //Valida o formato do CEP.
      if(validacep.test(cep)) {
        const { data } = await axios(`https://viacep.com.br/ws/${cep}/json/`);
        if (data && data.rua !== '') {
          const {bairro, cep, complemento, localidade, logradouro, uf} = data;
          setEndereco({...endereco, bairro, cep, complemento, localidade, logradouro, uf})
        }
      }
    }

  }

  return (
    <form onSubmit={handleSubmit} onReset={handleFormReset}>

      <div className="row">
        <img className="aluno-foto" src={fotoPerfil} alt="fotoAluno" />
      </div>

      <div className="input-block">
        <label htmlFor="usuario">Usuário</label>
        <input 
          name="usuario" 
          id="usuario" 
          required
          disabled={isEdit}
          value={aluno.usuario}
          onChange={e => setAluno({...aluno, usuario: e.target.value})}
        />
      </div>

      <div className="input-block">
        <label htmlFor="nome">Nome do aluno:</label>
        <input
          name="nome"
          id="nome"
          required
          value={aluno.nome}
          onChange={e => setAluno({...aluno, nome: e.target.value})}
        />
      </div>

      <div className="input-group">
        <div className="input-block">
          <label htmlFor="cep">CEP</label>
          <input 
            name="cep" 
            id="cep" 
            required 
            value={endereco.cep}
            onChange={e => {
              setEndereco({...endereco, cep: e.target.value})
              findCorreios(e.target.value)
            }}
          />
        </div>

        <div className="input-block">
          <label htmlFor="rua">Rua: </label>
          <input
            name="rua"
            id="rua"
            required
            value={endereco.logradouro}
            onChange={e => {
              setEndereco({...endereco, logradouro: e.target.value})
            }}
          />
        </div>

        <div className="input-block">
          <label htmlFor="complemento">Complemento: </label>
          <input
            name="complemento"
            id="complemento"
            value={endereco.complemento}
            onChange={e => {
              setEndereco({...endereco, complemento: e.target.value})
            }}
          />
        </div>


        <div className="input-block">
          <label htmlFor="bairro">Número: </label>
          <input
            type="number"
            name="numero"
            id="numero"
            required
            value={endereco.numero}
            onChange={e => {
              setEndereco({...endereco, numero: e.target.value})
            }}
          />
        </div>


        <div className="input-block">
          <label htmlFor="bairro">Bairro: </label>
          <input
            name="bairro"
            id="bairro"
            required
            value={endereco.bairro}
            onChange={e => {
              setEndereco({...endereco, bairro: e.target.value})
            }}
          />
        </div>

        <div className="input-block">
          <label htmlFor="cidade">Cidade: </label>
          <input
            name="cidade"
            id="cidade"
            required
            value={endereco.localidade}
            onChange={e => {
              setEndereco({...endereco, localidade: e.target.value})
            }}
          />
        </div>

        <div className="input-block">
          <label htmlFor="estado">Estado: </label>
          <input
            name="estado"
            id="estado"
            required
            value={endereco.uf}
            onChange={e => {
              setEndereco({...endereco, uf: e.target.value})
            }}
          />
        </div>
      </div>

      <div>
          <ImageUploader
            withIcon={true}
            buttonText='Selecione a imagem'
            buttonStyles={{'background': '#095'}}
            onChange={uploadFotoMethod}
            imgExtension={['.jpg']}
            maxFileSize={5242880}
            label='tipos aceitaveis: JPG | Tamanho maximo: 5mb'
            singleImage={true}
          />
        </div>

      <button type="submit">Salvar</button>
      <button type="reset">Limpar</button>
    </form>
  );
}

export default FormularioAluno;
