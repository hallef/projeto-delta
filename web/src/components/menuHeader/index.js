import React from 'react';
import { useHistory, Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUniversity, faUserGraduate } from '@fortawesome/free-solid-svg-icons'
import './styles.css';

const Menu = () => {
  const history = useHistory();
  function goTo(rota) {
    history.push(rota);
  }
  return (
    <div className="menu">
      <FontAwesomeIcon className="link" icon={faUniversity} />
      <Link className="link" to={"/"} >
        UniMinas</Link>
      <button className="bt-graduacao" onClick={() => goTo("/cadastro")}>
        <FontAwesomeIcon className="icon-student" icon={faUserGraduate} />
        <p className="text-button">Portal do Aluno</p></button>
    </div>
  )
}

export default Menu;