package com.uniminas.repository;

import com.uniminas.model.Aluno;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AlunoRepository extends JpaRepository<Aluno, Integer> {

    List<Aluno> findByNomeIgnoreCaseContaining(String nome);

}
