package com.uniminas.service;

import com.uniminas.model.Aluno;
import com.uniminas.model.Endereco;
import com.uniminas.repository.AlunoRepository;
import com.uniminas.repository.EnderecoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class AlunoService {

    private final AlunoRepository alunoRepository;
    private final EnderecoRepository enderecoRepository;

    public AlunoService(AlunoRepository alunoRepository, EnderecoRepository enderecoRepository) {
        this.alunoRepository = alunoRepository;
        this.enderecoRepository = enderecoRepository;
    }

    public List<Aluno> findAll() {
        return alunoRepository.findAll();
    }

    public List<Aluno> findAlunos(String nome) {
        return nome.isEmpty() ? alunoRepository.findAll() : alunoRepository.findByNomeIgnoreCaseContaining(nome);
    }

    public Aluno getById(Integer id) {
        return alunoRepository.findById(id).orElseThrow();
    }

    public void deleteById(Integer id) {
        alunoRepository.deleteById(id);
    }

    public Aluno updateAluno(Aluno aluno) {

        Aluno alunoSalvo = alunoRepository.findById(aluno.getId()).orElseThrow();
        Endereco enderecoSalvo = enderecoRepository.findById(aluno.getEndereco().getId()).orElseThrow();

        enderecoSalvo.setBairro(aluno.getEndereco().getBairro());
        enderecoSalvo.setCep(aluno.getEndereco().getCep());
        enderecoSalvo.setComplemento(aluno.getEndereco().getComplemento());
        enderecoSalvo.setLocalidade(aluno.getEndereco().getLocalidade());
        enderecoSalvo.setLogradouro(aluno.getEndereco().getLogradouro());
        enderecoSalvo.setNumero(aluno.getEndereco().getNumero());
        enderecoSalvo.setUf(aluno.getEndereco().getUf());
        enderecoRepository.save(alunoSalvo.getEndereco());

        alunoSalvo.setEndereco(aluno.getEndereco());

        alunoSalvo.setFoto(aluno.getFoto());
        alunoSalvo.setNome(aluno.getNome());
        alunoSalvo.setEndereco(aluno.getEndereco());
        alunoRepository.save(alunoSalvo);

        return alunoSalvo;

    }

    public Aluno createdAluno(Aluno aluno) {

        enderecoRepository.save(aluno.getEndereco());
        alunoRepository.save(aluno);

        return aluno;

    }
}
