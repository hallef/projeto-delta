package com.uniminas.controllers;

import com.uniminas.model.Aluno;
import com.uniminas.service.AlunoService;
import com.uniminas.utils.FiltroAluno;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/aluno")
public class AlunoController {

    private final AlunoService alunoService;

    public AlunoController(AlunoService alunoService) {
        this.alunoService = alunoService;
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(path = "/findAluno", produces = "application/json")
    public List<Aluno> alunos(@RequestBody FiltroAluno filtroAluno) {
        return alunoService.findAlunos(filtroAluno.nome);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/getAlunos", produces = "application/json")
    public List<Aluno> alunos() {
        return alunoService.findAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/{id}")
    public Aluno getAluno(@PathVariable Integer id) {
        return alunoService.getById(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("deletar/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
        alunoService.deleteById(id);
        return ResponseEntity.ok("ok");
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(path = "/update")
    public Aluno updateAluno(@RequestBody Aluno aluno) {

        if (aluno == null) {
            new Throwable("Aluno não encontrado");
        }

        return alunoService.updateAluno(aluno);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/created")
    public Aluno createdAluno(@RequestBody Aluno aluno) {

        if (aluno == null) {
            new Throwable("Aluno nulo");
        }

        return alunoService.createdAluno(aluno);
    }

}
