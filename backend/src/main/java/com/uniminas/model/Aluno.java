package com.uniminas.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(schema = "public")
public class Aluno {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "aluno_id_seq")
    @SequenceGenerator(name = "aluno_id_seq", sequenceName = "public.aluno_id_seq", allocationSize = 1)
    private Integer id;

    @Column(name = "usuario")
    private String usuario;

    @Column(name = "nome")
    private String nome;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_endereco", referencedColumnName = "id")
    private Endereco endereco;

    @Column(name = "foto")
    private String foto;
}
